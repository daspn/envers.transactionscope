﻿using System;

namespace TransactionScope.Envers.Tests
{
    public interface IEntity<TKey>
    {
        TKey Id { get; }
    }

    public abstract class AbstractEntity<TKey> : IEntity<TKey>
    {
        public virtual TKey Id { get; private set; }

        public virtual int Version { get; private set; }
    }

    public class TestEntity : AbstractEntity<Guid>
    {
        public virtual int Property1 { get; set; }

        public virtual string Property2 { get; set; }
    }
}