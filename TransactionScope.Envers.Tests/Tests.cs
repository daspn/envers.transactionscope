﻿using System;
using System.IO;
using System.Linq;
using System.Xml;
using FluentNHibernate.Cfg;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Envers;
using NUnit.Framework;

namespace TransactionScope.Envers.Tests
{
    [TestFixture]
    public class Tests
    {
        [Test]
        public void AuditUsingTransactionScope()
        {
            using (ISessionFactory sf = GetSessionFactory())
            {
                Guid entityId;
                using (ISession s = sf.OpenSession())
                {
                    //create an entity
                    using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
                    {
                        var newEntity = new TestEntity { Property1 = 100, Property2 = "test" };
                        s.Save(newEntity);
                        entityId = newEntity.Id;

                        scope.Complete();
                    }
                }

                using (ISession s = sf.OpenSession())
                {
                    //update the entity
                    using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
                    {
                        var entityToUpdate = s.Get<TestEntity>(entityId);
                        entityToUpdate.Property1 = 200;
                        s.Update(entityToUpdate);

                        scope.Complete();
                    }
                }

                using (ISession s = sf.OpenSession())
                {
                    //delete the entity
                    using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
                    {
                        var entityToDelete = s.Get<TestEntity>(entityId);
                        s.Delete(entityToDelete);

                        scope.Complete();
                    }
                }

                using (ISession s = sf.OpenSession())
                {
                    IAuditReader auditer = AuditReaderFactory.Get(s);
                    var results = auditer.CreateQuery().ForRevisionsOf<TestEntity>(true).Results();
                    Assert.AreEqual(3, results.Count());
                }
            }
        }

        [Test]
        public void AuditUsingNHibernateTransactions()
        {
            using (ISessionFactory sf = GetSessionFactory())
            {
                Guid entityId;
                using (ISession s = sf.OpenSession())
                {
                    //create an entity
                    using (ITransaction t = s.BeginTransaction())
                    {
                        var newEntity = new TestEntity { Property1 = 100, Property2 = "test" };
                        s.Save(newEntity);
                        entityId = newEntity.Id;

                        t.Commit();
                    }
                }

                using (ISession s = sf.OpenSession())
                {
                    //update the entity
                    using (ITransaction t = s.BeginTransaction())
                    {
                        var entityToUpdate = s.Get<TestEntity>(entityId);
                        entityToUpdate.Property1 = 200;
                        s.Update(entityToUpdate);

                        t.Commit();
                    }
                }

                using (ISession s = sf.OpenSession())
                {
                    //delete the entity
                    using (ITransaction t = s.BeginTransaction())
                    {
                        var entityToDelete = s.Get<TestEntity>(entityId);
                        s.Delete(entityToDelete);

                        t.Commit();
                    }
                }

                using (ISession s = sf.OpenSession())
                {
                    IAuditReader auditer = AuditReaderFactory.Get(s);
                    var results = auditer.CreateQuery().ForRevisionsOf<TestEntity>(true).Results();
                    Assert.AreEqual(3, results.Count());
                }
            }
        }

        private static ISessionFactory GetSessionFactory()
        {
            var configuration = new Configuration();
            string hibernateConfig = "hibernate.cfg.xml";

            if (Path.IsPathRooted(hibernateConfig) == false)
                hibernateConfig = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, hibernateConfig);
            if (File.Exists(hibernateConfig))
                configuration.Configure(new XmlTextReader(hibernateConfig));

            return Fluently.Configure(configuration)
                .Mappings(m =>
                {
                    m.FluentMappings.Add<TestEntityMap>();
                })
                .ExposeConfiguration(cfg =>
                {
                    var enversConfiguration = new NHibernate.Envers.Configuration.Fluent.FluentConfiguration();
                    enversConfiguration.Audit<TestEntity>();
                    cfg.IntegrateWithEnvers(enversConfiguration);

                    new NHibernate.Tool.hbm2ddl.SchemaExport(cfg).Execute(false, true, false);
                })
                .BuildSessionFactory();
        }
    }
}