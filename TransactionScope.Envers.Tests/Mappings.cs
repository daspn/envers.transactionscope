﻿using System;
using FluentNHibernate.Mapping;

namespace TransactionScope.Envers.Tests
{
    public class TestEntityMap : ClassMap<TestEntity>
    {
        public TestEntityMap()
        {
            Id(x => x.Id);
            Version(x => x.Version);
            Map(x => x.Property1);
            Map(x => x.Property2);
        }
    }
}